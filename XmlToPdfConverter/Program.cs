﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using VVX;

namespace XmlToPdfConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            var fileLocation = ConfigurationSettings.AppSettings["fileLocation"];
            var path = ConfigurationSettings.AppSettings["path"];

            var fileName =   System.IO.File.ReadAllText(fileLocation);
            var xmlPath = $"{path}{fileName}";

            var pdfName = fileName.Replace(".xml", ".pdf");
            var pdfPath = $"{path}{pdfName}";

            PDF pdf = new PDF();

            pdf.DoCreateFromXmlStore(pdfPath, xmlPath);
        }
    }
}
