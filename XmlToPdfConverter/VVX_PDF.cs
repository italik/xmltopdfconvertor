using System;
using System.IO;

using System.Diagnostics;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Font = iTextSharp.text.Font;
using Image = iTextSharp.text.Image;
using Rectangle = iTextSharp.text.Rectangle;

namespace VVX
{
    /// <summary>
    /// Wrapper to access iTextSharp
    /// </summary>
   public class PDF
    {
        #region ENUMs
        public enum PaperSize
        {
            LetterUS,
            LegalUS,
            A4,
                // a large number of other PDF sizes are not listed here.
                // If you add any, be sure to update DoCvtToPageSize(...)
            _COUNT  //must be last
        }

        public enum TypeFace
        {
            Times,
            Arial,
            Courier,

            _COUNT  //must be last
        }

        public enum ViewLayout
        {
            OnePage = PdfWriter.PageLayoutSinglePage,
            TwoPage = PdfWriter.PageLayoutTwoColumnLeft
            //PdfWriter.PageLayoutSinglePage - Display one page at a time. 
            //PdfWriter.PageLayoutOneColumn - Display the pages in one column. 
            //PdfWriter.PageLayoutTwoColumnLeft - Display the pages in two columns, with oddnumbered pages on the left. 
            //PdfWriter.PageLayoutTwoColumnRight - Display the pages in two columns, with oddnumbered pages on the right. 
        }

        public enum ViewMode
        {
            Default
            //PdfWriter.PageModeUseNone - Neither document outline nor thumbnail images visible. 
            //PdfWriter.PageModeUseOutlines - Document outline visible. 
            //PdfWriter.PageModeUseThumbs - Thumbnail images visible. 
            //PdfWriter.PageModeFullScreen - Full-screen mode, with no menu bar, window controls, or any other window visible. 
        }
        #endregion //ENUMs

        #region MEMBER VARIABLEs

        private string msFilePDF = "";
        
        private string msMsg = "";
        private bool mbRet = false;
        private string msEOL = Environment.NewLine;

        private float mfWidthScaleFactor = 1.0f;
        private bool mbApplyAlternatingColors = !true;

        //---------------------------------------------------------------
        // Document Summary
        //---------------------------------------------------------------
        private string msDocTitle = "";
        private string msDocAuthor = "";
        private string msDocSubject = "";
        private string msDocKeywords = "";

        //---------------------------------------------------------------
        // View Options
        //---------------------------------------------------------------
        private bool mbView2PageLayout = false;
        private bool mbViewToolbar = true;
        private bool mbViewMenubar = true;
        private bool mbViewWindowUI = true;
        private bool mbViewResizeToFit = true;
        private bool mbViewCenterOnScreen = true;
        private PaperSize menShowPaperSize = PaperSize.LetterUS;

        //---------------------------------------------------------------
        // Show Options
        //---------------------------------------------------------------
        private bool mbShowTitle = true;
        private bool mbShowPageNumber = true;
        private bool mbShowWatermark = true;
        private string msShowWatermarkText = "WATERMARK";
        private string msShowWatermarkFile = "watermark.png";
        private bool mbShowLandscape = true;

        //---------------------------------------------------------------
        // TypeFace Options
        //---------------------------------------------------------------
        private TypeFace menBodyTypeFace = TypeFace.Arial;
        private float mfBodyTypeSize = 10f;
        private bool mbBodyTypeStyleBold = false;
        private bool mbBodyTypeStyleItalics = false;

        private TypeFace menHeaderTypeFace = TypeFace.Arial;
        private float mfHeaderTypeSize = 10f;
        private bool mbHeaderTypeStyleBold = true;
        private bool mbHeaderTypeStyleItalics = false;

        //---------------------------------------------------------------
        // Permissions Options
        //---------------------------------------------------------------
        private bool mbEncryptionNeeded = false;
        private string msEncryptionPasswordOfCreator = "";
        private string msEncryptionPasswordOfReader = "";
        private bool mbEncryptionStrong = false;

        private bool mbAllowPrinting = true;
        private bool mbAllowModifyContents = true;
        private bool mbAllowCopy = true;
        private bool mbAllowModifyAnnotations = true;
        private bool mbAllowFillIn = true;
        private bool mbAllowScreenReaders = true;
        private bool mbAllowAssembly = true;
        private bool mbAllowDegradedPrinting = true;



        #endregion //MEMBER VARIABLEs

        #region PROPERTIES
        public PaperSize ShowPaperSize
        {
            get { return menShowPaperSize; }
            set { menShowPaperSize = value; }
        }

        public bool View2PageLayout
        {
            get { return mbView2PageLayout; }
            set { mbView2PageLayout = value; }
        }

        public bool ViewToolbar
        {
            get { return mbViewToolbar; }
            set { mbViewToolbar = value; }
        }

        public bool ViewMenubar
        {
            get { return mbViewMenubar; }
            set { mbViewMenubar = value; }
        }

        public bool ViewWindowUI
        {
            get { return mbViewWindowUI; }
            set { mbViewWindowUI = value; }
        }

        public bool ViewResizeToFit
        {
            get { return mbViewResizeToFit; }
            set { mbViewResizeToFit = value; }
        }

        public bool ViewCenterOnScreen
        {
            get { return mbViewCenterOnScreen; }
            set { mbViewCenterOnScreen = value; }
        }

        public bool ShowTitle
        {
            get { return mbShowTitle; }
            set { mbShowTitle = value; }
        }

        public bool ShowPageNumber
        {
            get { return mbShowPageNumber; }
            set { mbShowPageNumber = value; }
        }

        public bool ShowWatermark
        {
            get { return mbShowWatermark; }
            set { mbShowWatermark = value; }
        }

        public string ShowWatermarkText
        {
            get { return msShowWatermarkText; }
            set { msShowWatermarkText = value; }
        }

        public string ShowWatermarkFile
        {
            get { return msShowWatermarkFile; }
            set { msShowWatermarkFile = value; }
        }

        public bool ShowLandscape
        {
            get { return mbShowLandscape; }
            set { mbShowLandscape = value; }
        }


        //---------------------------------------------------------------
        // 
        //---------------------------------------------------------------
        public bool ApplyAlternatingColors
        {
            get { return mbApplyAlternatingColors; }
            set { mbApplyAlternatingColors = value; }
        }

        public string DocTitle
        {
            get { return msDocTitle; }
            set { msDocTitle = value; }
        }

        public string DocAuthor
        {
            get { return msDocAuthor; }
            set { msDocAuthor = value; }
        }

        public string DocSubject
        {
            get { return msDocSubject; }
            set { msDocSubject = value; }
        }

        public string DocKeywords
        {
            get { return msDocKeywords; }
            set { msDocKeywords = value; }
        }

        public float WidthScaleFactor
        {
            get { return mfWidthScaleFactor; }
            set { mfWidthScaleFactor = value; }
        }

        public string Message
        {
            get { return msMsg; }
            set { msMsg = value; }
        }

        public bool Success
        {
            get { return mbRet; }
            set { mbRet = value; }
        }

        public string Filename
        {
            get { return msFilePDF; }
            set { msFilePDF = value; }
        }

        public TypeFace FontBodyTypeFace
        {
            get { return menBodyTypeFace; }
            set { menBodyTypeFace = value; }
        }

        public float FontBodyTypeSize
        {
            get { return mfBodyTypeSize; }
            set { mfBodyTypeSize = value; }
        }

        public bool FontBodyTypeStyleBold
        {
            get { return mbBodyTypeStyleBold; }
            set { mbBodyTypeStyleBold = value; }
        }

        public bool FontBodyTypeStyleItalics
        {
            get { return mbBodyTypeStyleItalics; }
            set { mbBodyTypeStyleItalics = value; }
        }

        internal TypeFace FontHeaderTypeFace
        {
            get { return menHeaderTypeFace; }
            set { menHeaderTypeFace = value; }
        }

        public float FontHeaderTypeSize
        {
            get { return mfHeaderTypeSize; }
            set { mfHeaderTypeSize = value; }
        }

        public bool FontHeaderTypeStyleBold
        {
            get { return mbHeaderTypeStyleBold; }
            set { mbHeaderTypeStyleBold = value; }
        }

        public bool FontHeaderTypeStyleItalics
        {
            get { return mbHeaderTypeStyleItalics; }
            set { mbHeaderTypeStyleItalics = value; }
        }

        public bool EncryptionNeeded
        {
            get { return mbEncryptionNeeded; }
            set { mbEncryptionNeeded = value; }
        }

        public string EncryptionPasswordOfCreator
        {
            get { return msEncryptionPasswordOfCreator; }
            set { msEncryptionPasswordOfCreator = value; }
        }

        public string EncryptionPasswordOfReader
        {
            get { return msEncryptionPasswordOfReader; }
            set { msEncryptionPasswordOfReader = value; }
        }

        public bool EncryptionStrong
        {
            get { return mbEncryptionStrong; }
            set { mbEncryptionStrong = value; }
        }

        public bool AllowPrinting
        {
            get { return mbAllowPrinting; }
            set { mbAllowPrinting = value; }
        }

        public bool AllowModifyContents
        {
            get { return mbAllowModifyContents; }
            set { mbAllowModifyContents = value; }
        }

        public bool AllowCopy
        {
            get { return mbAllowCopy; }
            set { mbAllowCopy = value; }
        }

        public bool AllowModifyAnnotations
        {
            get { return mbAllowModifyAnnotations; }
            set { mbAllowModifyAnnotations = value; }
        }

        public bool AllowFillIn
        {
            get { return mbAllowFillIn; }
            set { mbAllowFillIn = value; }
        }

        public bool AllowScreenReaders
        {
            get { return mbAllowScreenReaders; }
            set { mbAllowScreenReaders = value; }
        }

        public bool AllowAssembly
        {
            get { return mbAllowAssembly; }
            set { mbAllowAssembly = value; }
        }

        public bool AllowDegradedPrinting
        {
            get { return mbAllowDegradedPrinting; }
            set { mbAllowDegradedPrinting = value; }
        }

        #endregion //PROPERTIES
        
        #region Helper methods
     
    
        /// <summary>
        /// Convert the pseudo Windows-like type face names into those that
        /// the PDF and iTextSharp understand.
        /// </summary>
        /// <param name="enTypeFace">Windows-like type face names, e.g., "Arial"</param>
        /// <returns>type face name PDF and iTextSharp understand</returns>
        private string DoCvtToFontName(TypeFace enTypeFace)
        {
            string fontName;
            switch (enTypeFace)
            {
                default:
                case TypeFace.Arial:
                    fontName = FontFactory.HELVETICA;
                    break;
                case TypeFace.Times:
                    fontName = FontFactory.TIMES_ROMAN;
                    break;
                case TypeFace.Courier:
                    fontName = FontFactory.COURIER;
                    break;
            }

            return fontName;
        }

        /// <summary>
        /// Combines the caller/UI specified element-specific flags
        /// (e.g., for the Body or Header) into an 'int' value used by iTextSharp
        /// in creating the appropriate 'Font'
        /// </summary>
        /// <param name="bBold">true if font needs to be Bold</param>
        /// <param name="bItalics">true if font needs to be Italics</param>
        /// <returns></returns>
        private int DoCvtToStyle(bool bBold, bool bItalics)
        {
            int fontStyle = 0;
            if (bBold)
                fontStyle |= Font.BOLD;
            if (bItalics)
                fontStyle |= Font.ITALIC;
            return fontStyle;
        }

        /// <summary>
        /// Sets the Viewer Preferences based on values set by the caller
        /// NOTE: Must be called BEFORE calling iTextSharp.document.Open();
        /// </summary>
        /// <param name="writer">the writer that will embed the preferences</param>
        /// <returns></returns>
        private bool DoSetViewerPreferences(PdfWriter writer)
        {
            bool bRet = true;

            int nViewerPreferences = 0;
            
            if (this.mbView2PageLayout)
                nViewerPreferences |= PdfWriter.PageLayoutTwoColumnLeft;
            
            if (this.mbViewCenterOnScreen)
                nViewerPreferences |= PdfWriter.CenterWindow;
            
            if (!this.mbViewMenubar)
                nViewerPreferences |= PdfWriter.HideMenubar;
            
            if (this.mbViewResizeToFit)
                nViewerPreferences |= PdfWriter.FitWindow;
            
            if (!this.mbViewToolbar)
                nViewerPreferences |= PdfWriter.HideToolbar;
            
            if (!this.mbViewWindowUI)
                nViewerPreferences |= PdfWriter.HideWindowUI;
            
            writer.ViewerPreferences = nViewerPreferences;

            return bRet;
        }

        /// <summary>
        /// Sets the Viewer Permissions based on values set by the caller
        /// various member variables.
        /// NOTE: Must be called BEFORE calling iTextSharp.document.Open();
        /// </summary>
        /// <param name="writer">the writer that will embed the permissions</param>
        /// <returns>true</returns>
        private bool DoSetViewerPermissions(PdfWriter writer)
        {
            bool bRet = true;

            //--- init the container for the various bit flags
            int permissions = 0;

            if (this.mbAllowPrinting)
                permissions |= PdfWriter.AllowPrinting;
            else
                this.mbEncryptionNeeded = true;

            if (this.mbAllowModifyContents)
                permissions |= PdfWriter.AllowModifyContents;
            else
                this.mbEncryptionNeeded = true;

            if (this.mbAllowCopy)
                permissions |= PdfWriter.AllowCopy;
            else
                this.mbEncryptionNeeded = true;

            if (this.mbAllowModifyAnnotations)
                permissions |= PdfWriter.AllowModifyAnnotations;
            else
                this.mbEncryptionNeeded = true;

            if (this.mbAllowFillIn)
                permissions |= PdfWriter.AllowFillIn;
            else
                this.mbEncryptionNeeded = true;

            if (this.mbAllowScreenReaders)
                permissions |= PdfWriter.AllowScreenReaders;
            else
                this.mbEncryptionNeeded = true;

            if (this.mbAllowAssembly)
                permissions |= PdfWriter.AllowAssembly;
            else
                this.mbEncryptionNeeded = true;

            if (this.mbAllowDegradedPrinting)
                permissions |= PdfWriter.AllowDegradedPrinting;
            else
                this.mbEncryptionNeeded = true;

            // NOTE: Even if the caller has not set passwords, for any of
            // the restrictions to take effect, the PDF has to be encrypted
            // See http://itextsharp.sourceforge.net/tutorial/ch01.html for details
            if (this.mbEncryptionNeeded)
            {
                writer.SetEncryption(this.mbEncryptionStrong
                                    , this.msEncryptionPasswordOfReader
                                    , this.msEncryptionPasswordOfCreator
                                    , permissions);
            }

            return bRet;
        }

        /// <summary>
        /// Adds Meta Data to the PDF document
        /// NOTE: Must be called BEFORE calling iTextSharp.document.Open();
        /// </summary>
        /// <param name="document">the iTextWriter document in which the metat data will be embedded</param>
        /// <returns>true, if successful</returns>
        private bool DoAddMetaData(Document document)
        {
            bool bRet = true;

            if (document != null)
            {
                // step 3: we add some metadata and open the document
                document.AddTitle(this.msDocTitle);
                document.AddSubject(this.msDocSubject);
                document.AddKeywords(this.msDocKeywords);
                document.AddCreator("VVX.PDF");
                document.AddAuthor(this.msDocAuthor);
                document.AddHeader("Expires", "0");
            }
            else
            {
                bRet = false;
            }

            return bRet;
        }

        /// <summary>
        /// Maps the VVX.PDF PaperSize to the corresponding 'Rectangle' value in iTextSharp
        /// </summary>
        /// <param name="enPaperSize">VVX.PDF PaperSize value</param>
        /// <returns>'Rectangle' value used by iTextSharp to define the size of a page</returns>
        private Rectangle DoCvtToPageSize(PaperSize enPaperSize)
        {
            Rectangle pageSize;
            switch (enPaperSize)
            {
                default:
                    pageSize = PageSize.LETTER;
                    break;
                case PaperSize.LegalUS:
                    pageSize = PageSize.LEGAL;
                    break;
                case PaperSize.A4:
                    pageSize = PageSize.A4;
                    break;
            }

            return pageSize;
        }

        #endregion //Helper methods

        #region XmlStore

        /// <summary>
        /// Create a PDF Table file from an XmlStore (or a 'Plain Vanilla' XML file)
        /// </summary>
        /// <param name="sFilePDF">Fully qualified name of the PDF file to be created</param>
        /// <param name="sXmlStoreFileIn">Fully qualified name of the 'input' XML file containing the tabular data</param>
        /// <returns>'true' if successful</returns>
        public bool DoCreateFromXmlStore(string sFilePDF, string sXmlStoreFileIn)
        {
            this.mbRet = false;

           // Debug.WriteLine("CreateFromXmlStore: " + sXmlStoreFileIn);

            // step 1: creation of a document-object
            // creation of the document with a certain size and certain margins
            // NOTE: We will use the default margins (36 pts on each edge)

            Rectangle mPageSize = this.DoCvtToPageSize (this.menShowPaperSize);

            //--- if landscape, then rotate the page (90 degrees)
            //if (this.mbShowLandscape)
            //    mPageSize = mPageSize.Rotate();

            //--- NOTE: Use one of the other constructors to set the page color and margins
            //    this one just takes the default color and margins.
            Document document = new Document(PageSize.A4.Rotate());

            try
            {
                // step 2:
                // we create a writer that listens to the document
                // and directs a PDF-stream to a file
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(sFilePDF, FileMode.Create));

                // step 2a: BEFORE we open the document, MUST set "view" preferences 
                this.DoSetViewerPreferences(writer);

                // step 2b: BEFORE we open the document, MUST set "view" permissions 
                this.DoSetViewerPermissions(writer);

                // step 2c: BEFORE we open the document, MUST add meta data
               // this.DoAddMetaData(document);

                // step 3x: now open the document
                document.Open();

                // step 4: we add content to the document (this happens in a separate method)
                // NOTE: If you want to use another datasource, you will need to
                //       write an appropriate DoLoadDocument(...) 
                //       along the same lines as this one
                this.DoLoadDocument(document, sXmlStoreFileIn);

                this.mbRet = true;
            }
            catch (DocumentException de)
            {
                this.Message = de.Message;
            }
            catch (IOException ioe)
            {
                this.Message = ioe.Message;
            }

            // step 5: we close the document
            document.Close();

            if (this.mbRet)
                this.Message = sFilePDF + " has been created";

            return this.mbRet;
        }

        /// <summary>
        /// Helper for DoCreateFromXmlStore(...). 
        /// Loads data from an XmlStore (or 'Plain Vanilla' XML) file into
        /// the iTextSharp document. 
        /// NOTE: if you want to load data from some other source, clone this method and
        /// write code specific to that data source, (i.e., replace the XmlStore-specific code)
        /// but generally follow the pattern used here.
        /// </summary>
        /// <param name="document">the target iTextSharp document</param>
        /// <param name="sXmlStoreFile">the source XmlStore (or 'Plain Vanilla' XML) file</param>
        /// <returns>'true' if successful</returns>
        private bool DoLoadDocument(Document document, string sXmlStoreFile)
        {
            bool bRet = false;

            try
            {
                int numRecordsInXml = 0;
                int numColumnsInXml = 0;
                bool bExcludeIdColumn = true;
                int BODY = 0;   //index for font
                int HDR = 1;   //index for font

                if (sXmlStoreFile.Length > 0)
                {
                    //--- create an instance of XmlStore
                    VVX.XmlStore xstore = new XmlStore(sXmlStoreFile);

                    //--- load the data from the Xml file
                    numRecordsInXml = xstore.DoLoadRecords();

                    numColumnsInXml = xstore.Fields.Length;

                    if (numRecordsInXml > 0 && numColumnsInXml > 0)
                    {
                        int numColumnsInPDF = numColumnsInXml;
                        if (bExcludeIdColumn)
                            numColumnsInPDF = numColumnsInXml - 1;

                        // as we have data, we can create a PDFPTable
                        PdfPTable datatable = new PdfPTable(numColumnsInPDF);

                        // define the column headers, sizes, etc.
                        datatable.DefaultCell.Padding = 3;  //in Points

                        //------------------------------------------------------------
                        // Set Column Widths
                        //------------------------------------------------------------
                        //--- set the relative width of each column

                        float[] columnWidthInPct = new float[numColumnsInPDF];
                        int col;

                        //--- see if we have width data for the Fields in XmlStore
                        float widthTotal = xstore.DoGetColumnWidthsTotal();
                        for (col = 0; col < numColumnsInPDF; col++)
                        {
                            if (widthTotal == 0f)
                            {
                                //--- equal widths (UGH!)
                                columnWidthInPct[col] = 100f / (float)numColumnsInPDF;
                            }
                            else
                            {
                                float widthCol = xstore.DoGetColumnWidth(col);
                                columnWidthInPct[col] = widthCol;
                            }
                        }
                        //--- set the total width of the table
                        if (mfWidthScaleFactor <= 0 || widthTotal == 0f)
                            datatable.WidthPercentage = 100; // percentage
                        else
                            datatable.WidthPercentage = widthTotal * mfWidthScaleFactor; // percentage

                        datatable.SetWidths(columnWidthInPct);

                        //------------------------------------------------------------
                        // Init fonts to be used
                        //------------------------------------------------------------
                        Font[] fonts = new Font[2];
                        string fontName = this.DoCvtToFontName(this.menBodyTypeFace);
                        float fontSize = this.mfBodyTypeSize;

                        int fontStyle = 0;
                        if (this.mbBodyTypeStyleBold)
                            fontStyle |= Font.BOLD;
                        if (this.mbBodyTypeStyleItalics)
                            fontStyle |= Font.ITALIC;
                        fonts[0] = FontFactory.GetFont(this.DoCvtToFontName(this.menBodyTypeFace),
                                                       this.mfBodyTypeSize,
                                                       this.DoCvtToStyle(this.mbBodyTypeStyleBold,
                                                                         this.mbBodyTypeStyleItalics)
                                                       );
                        fonts[1] = FontFactory.GetFont(this.DoCvtToFontName(this.menHeaderTypeFace),
                                                       this.mfHeaderTypeSize,
                                                       this.DoCvtToStyle(this.mbHeaderTypeStyleBold,
                                                                         this.mbHeaderTypeStyleItalics)
                                                       );


                        //------------------------------------------------------------
                        // Set Column Header Cell Attributes
                        //------------------------------------------------------------
                        datatable.DefaultCell.BorderWidth = 1;
                        datatable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;

                        //------------------------------------------------------------
                        // Set Column Header Text
                        //------------------------------------------------------------
                        //int row = 0;
                        for (col = 0; col < numColumnsInXml; col++)
                        {
                            if (bExcludeIdColumn)
                                if (col == xstore.ColumnUID)
                                    continue;

                            string sHdr = xstore.Fields[col].title;
                            Chunk chunk = new Chunk(sHdr, fonts[HDR]);
                            Phrase phrase = new Phrase(chunk);
                            datatable.AddCell(phrase);
                        }
                        datatable.HeaderRows = 1;  

                        //------------------------------------------------------------
                        // Set the Data (i.e., rows)
                        //------------------------------------------------------------
                        //--- now add the rows of data
                        for (int row = 0; row < numRecordsInXml; row++)
                        {
                            // NOTE: if mbApplyAlternatingColors is 'true'
                            //       the fill may cover any watermarks you want
                            if (mbApplyAlternatingColors && (row % 2 == 1))
                            {
                                datatable.DefaultCell.GrayFill = 0.9f;  //very light gray
                            }

                            string[] bogusData = xstore.DoGetRecord(row);
                            for (col = 0; col < numColumnsInXml; col++)
                            {
                                if (bExcludeIdColumn)
                                    if (col == xstore.ColumnUID)
                                        continue;

                                string sText = bogusData[col];
                                Chunk chunk = new Chunk(sText, fonts[BODY]);
                                Phrase phrase = new Phrase(chunk);
                                datatable.AddCell(phrase);
                            }

                            // NOTE: if mbApplyAlternatingColors is 'true'
                            //       the fill will cover any watermarks you want
                            if (mbApplyAlternatingColors && (row % 2 == 1))
                            {
                                datatable.DefaultCell.GrayFill = 1.0f;  //white
                            }
                        }

                        //------------------------------------------------------------
                        // create an event handler instance
                        // This event handler does page-specific "drawing" and "painting"
                        // such as drawing the "title" in the header, the page number in the footer
                        // the watermark, and the gray background for the column header
                        //------------------------------------------------------------
                        VVX.XmlStoreEvent pageEvent = new XmlStoreEvent();
                        // configure it
                        this.DoConfigPageEventHandler(pageEvent);

                        // set the TableEvent to communicate with the event handler
                        datatable.TableEvent = pageEvent;

                        //------------------------------------------------------------
                        // Add the table to the PDF document
                        //------------------------------------------------------------
                        document.Add(datatable);

                        // let the caller know we successfully reached 'the end' of this 
                        // request, i.e. loading the data into the iTextSharp 'document'

                        bRet = true;
                    }
                }
            }
            catch (Exception e)
            {
                this.Message += e.StackTrace;
                Debug.WriteLine(this.Message);
            }

            if (bRet == false)
            {
                document.Add(new Paragraph("Failed to load data from" + sXmlStoreFile));
            }

            return bRet;
        }

        /// <summary>
        /// This event handler does page-specific "drawing" and "painting"
        /// such as drawing the "title" in the header, the page number in the footer
        /// the watermark, and the gray background for the column header
        /// </summary>
        /// <param name="pageEvent">The custom event handler to be configured</param>
        private void DoConfigPageEventHandler(VVX.XmlStoreEvent pageEvent)
        {
            // --- page title (in header)
            if (this.ShowTitle)
            {
                pageEvent.PageTitle = this.msDocTitle;
            }

            // --- page number (in footer)
            if (this.ShowPageNumber)
            {
                pageEvent.PageNumberFormat = "Page {0}"; //default
                pageEvent.PageNumberStartingValue = 0;
            }

            // --- watermark
            //if (this.ShowWatermark)
            //{
            //    if (this.ShowWatermarkText.Length > 0)
            //        pageEvent.WatermarkText = this.ShowWatermarkText;

            //    if (this.ShowWatermarkFile.Length > 0)
            //        pageEvent.WatermarkFile = this.ShowWatermarkFile;
            //}

        }

        #endregion //XmlStore

        #region Helpers for selected Tutorials examples
        //See http://itextsharp.sourceforge.net/tutorial/ch05.html
        public bool Chap0518(string sFilePDF)
        {
            mbRet = false;

            Debug.WriteLine("Chapter 5 example 18: PdfPTable");

            // step 1: creation of a document-object
            // creation of the document with a certain size and certain margins

            Document document = new Document(PageSize.LETTER.Rotate(), 10, 10, 10, 10);

            try
            {
                // step 2:
                // we create a writer that listens to the document
                // and directs a PDF-stream to a file
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(sFilePDF, FileMode.Create));

                // step 3: we open the document
                document.Open();

                //Adds content to the document:
                // step 4: we add content to the document (this happens in a seperate method)
                LoadDocument(document);

                mbRet = true;
            }
            catch (DocumentException de)
            {
                this.Message = de.Message;
            }
            catch (IOException ioe)
            {
                this.Message = ioe.Message;
            }

            // step 5: we close the document
            document.Close();

            if (mbRet)
                this.Message = sFilePDF + " has been created";

            return mbRet;
        }

        private void LoadDocument(Document document)
        {
            String[] bogusData = { "M0065920",
								 "SL",
								 "FR86000P xxxx yyyyy zzzz",
								 "PCGOLD",
								 "119000",
								 "96 06",
								 "2001-08-13",
								 "4350",
								 "6011648299",
								 "FLFLMTGP",
								 "153",
								 "119000.00"
							 };
            int NumColumns = 12;
            try
            {
                // we add some meta information to the document

                PdfPTable datatable = new PdfPTable(NumColumns);

                datatable.DefaultCell.Padding = 3;
                float[] headerwidths = { 9, 4, 8, 10, 8, 11, 9, 7, 9, 10, 4, 10 }; // percentage
                datatable.SetWidths(headerwidths);
                datatable.WidthPercentage = 100; // percentage

                //datatable.DefaultCell.s

                datatable.DefaultCell.BorderWidth = 2;
                datatable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                datatable.AddCell("Clock #");
                datatable.AddCell("Trans Type");
                datatable.AddCell("Cusip");
                datatable.AddCell("Long Name");
                datatable.AddCell("Quantity");
                datatable.AddCell("Fraction Price");
                datatable.AddCell("Settle Date");
                datatable.AddCell("Portfolio");
                datatable.AddCell("ADP Number");
                datatable.AddCell("Account ID");
                datatable.AddCell("Reg Rep ID");
                datatable.AddCell("Amt To Go ");

                datatable.HeaderRows = 1;  // this is the end of the table header

                datatable.DefaultCell.BorderWidth = 1;

                int max = 66;   //was 666;
                for (int i = 1; i < max; i++)
                {
                    if (i % 2 == 1)
                    {
                        datatable.DefaultCell.GrayFill = 0.9f;
                    }
                    for (int x = 0; x < NumColumns; x++)
                    {
                        datatable.AddCell(bogusData[x]);
                    }
                    if (i % 2 == 1)
                    {
                        datatable.DefaultCell.GrayFill = 1.0f;
                    }
                }
                document.Add(datatable);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
            }
        }
        public bool Chap1202(string sFile)
        {
            mbRet = false;

            Debug.WriteLine("Chapter 12 example 2: Table events");

            // step 1: creation of a document-object
            Document document = new Document(PageSize.LETTER);
            try
            {
                // step 2: we create a writer that listens to the document
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(sFile, FileMode.Create));
                // step 3: we open the document
                document.Open();

                // step 4: we add some content
                BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);

                //-------------------------------------------
                // table 1
                //-------------------------------------------

                // create an instance

                PdfPTable table = new PdfPTable(4);
                table.DefaultCell.Border = Rectangle.NO_BORDER;

                // add the cell values

                int kMax = 24;
                for (int k = 0; k < kMax; ++k)
                {
                    if (k != 0)
                        table.AddCell("" + k);
                    else
                        table.AddCell("This is an URL");
                }

                // create an event instance

                MyTableEvent evnt = new MyTableEvent();

                // set the TableEvent to communicate with the event handler
                table.TableEvent = evnt;

                table.TotalWidth = 300;

                // write table 1 at some position
                table.WriteSelectedRows(0, -1, 100, 600, writer.DirectContent);

                // add table 1 (default position)
                document.Add(table);
                document.NewPage();

                //-------------------------------------------
                // table 2
                //-------------------------------------------
                table = new PdfPTable(4);
                float fontSize = 12;
                table.DefaultCell.PaddingTop = bf.GetFontDescriptor(BaseFont.ASCENT, fontSize) - fontSize + 2;
                table.DefaultCell.Border = Rectangle.NO_BORDER;

                kMax = 500 * 4;
                for (int k = 0; k < kMax; ++k)
                {
                    if (k == 0)
                        table.AddCell(new Phrase("This is an URL", new Font(bf, fontSize)));
                    else
                        table.AddCell(new Phrase("" + k, new Font(bf, fontSize)));
                }
                table.TableEvent = evnt;
                table.HeaderRows = 3;
                document.Add(table);

                mbRet = true;
            }
            catch (Exception de)
            {
                Debug.WriteLine(de.Message);
                Debug.WriteLine(de.StackTrace);
            }
            // step 5: close the document
            document.Close();

            return mbRet;
        }

        public bool Chap1011(string sFile)
        {
            mbRet = false;

            Console.WriteLine("Chapter 10 example 11: a PdfPTable in a template");

            // step 1: creation of a document-object
            Rectangle rect = new Rectangle(PageSize.A4);
          //  rect.BackgroundColor = new Color(238, 221, 88);
            Document document = new Document(rect, 50, 50, 50, 50);
            try
            {
                // step 2: we create a writer that listens to the document
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(sFile, FileMode.Create));
                // step 3: we open the document
                document.Open();
                // step 4:
                PdfTemplate template = writer.DirectContent.CreateTemplate(20, 20);
                BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
                String text = "Vertical";
                float size = 16;
                float width = bf.GetWidthPoint(text, size);

                template.BeginText();
                template.SetRGBColorFillF(1, 1, 1);
                template.SetFontAndSize(bf, size);
                template.SetTextMatrix(0, 2);
                template.ShowText(text);
                template.EndText();
                template.Width = width;
                template.Height = size + 2;
                
                Image img = Image.GetInstance(template);
                img.Rotation = 90;
                Chunk ck = new Chunk(img, 0, 0);
                PdfPTable table = new PdfPTable(3);
                table.WidthPercentage = 100;
                table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                PdfPCell cell = new PdfPCell(img);
                cell.Padding = 4;
              //  cell.BackgroundColor = new Color(0, 0, 255);
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell("I see a template on my right");
                table.AddCell(cell);
                table.AddCell("I see a template on my left");
                table.AddCell(cell);
                table.AddCell("I see a template everywhere");
                table.AddCell(cell);
                table.AddCell("I see a template on my right");
                table.AddCell(cell);
                table.AddCell("I see a template on my left");

                Paragraph p1 = new Paragraph("This is a template ");
                p1.Add(ck);
                p1.Add(" just here.");
                p1.Leading = img.ScaledHeight * 1.1f;
                document.Add(p1);
                document.Add(table);
                Paragraph p2 = new Paragraph("More templates ");
                p2.Leading = img.ScaledHeight * 1.1f;
                p2.Alignment = Element.ALIGN_JUSTIFIED;
                img.ScalePercent(70);
                for (int k = 0; k < 20; ++k)
                    p2.Add(ck);
                document.Add(p2);

                mbRet = true;
            }
            catch (Exception de)
            {
                Debug.WriteLine(de.Message);
                Debug.WriteLine(de.StackTrace);
            }

            // step 5: we close the document
            document.Close();

            return mbRet;

        }

        #endregion //Tutorials

    }
}
